//
//  AppDelegate.h
//  ChatClient
//
//  Created by Peng Xie on 9/7/16.
//  Copyright © 2016 XP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

